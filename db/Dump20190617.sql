CREATE DATABASE  IF NOT EXISTS `coleccion_cds` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `coleccion_cds`;
-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: coleccion_cds
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `autores`
--

DROP TABLE IF EXISTS `autores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `autores` (
  `id_autor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_autor`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autores`
--

LOCK TABLES `autores` WRITE;
/*!40000 ALTER TABLE `autores` DISABLE KEYS */;
INSERT INTO `autores` VALUES (1,'Oasis'),(2,'Blurr'),(3,'Rozalén'),(4,'Georgina León'),(5,'Devin Townsend'),(6,'Je Veux'),(7,'Celine Dion'),(8,'Rammstein'),(9,'Ebri Knight'),(10,'Mélnitsa'),(11,'Huun‐Huur‐Tu'),(12,'The Hatters'),(13,'Of Monsters and Men'),(14,'Spice Girls'),(15,'Got7'),(16,'Seventeen'),(17,'Hellowen'),(18,'Paco de Lucía');
/*!40000 ALTER TABLE `autores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autores_cds`
--

DROP TABLE IF EXISTS `autores_cds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `autores_cds` (
  `id_autor` int(11) NOT NULL,
  `id_cd` int(11) NOT NULL,
  PRIMARY KEY (`id_autor`,`id_cd`),
  KEY `FK_AUTORES_CDS_CDS_idx` (`id_cd`),
  CONSTRAINT `FK_AUTORES_CDS_AUTORES` FOREIGN KEY (`id_autor`) REFERENCES `autores` (`id_autor`),
  CONSTRAINT `FK_AUTORES_CDS_CDS` FOREIGN KEY (`id_cd`) REFERENCES `cds` (`id_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autores_cds`
--

LOCK TABLES `autores_cds` WRITE;
/*!40000 ALTER TABLE `autores_cds` DISABLE KEYS */;
INSERT INTO `autores_cds` VALUES (1,1),(1,2),(2,3),(2,4),(3,5),(3,6),(4,7),(4,8),(5,9),(5,10),(6,11),(6,12),(7,13),(7,14),(8,15),(8,16),(9,17),(9,18),(10,19),(10,20),(11,21),(11,22),(12,23),(12,24),(13,25),(13,26),(14,27),(14,28),(15,29),(16,30),(16,31),(17,32),(17,33),(18,34),(1,37),(2,37);
/*!40000 ALTER TABLE `autores_cds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cds`
--

DROP TABLE IF EXISTS `cds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cds` (
  `id_cd` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) DEFAULT NULL,
  `id_comprador` int(11) DEFAULT NULL,
  `id_idioma` int(11) DEFAULT NULL,
  `id_discografica` int(11) DEFAULT NULL,
  `anno` year(4) DEFAULT NULL,
  PRIMARY KEY (`id_cd`),
  KEY `FK_COMPRADORES_idx` (`id_comprador`),
  KEY `FK_IDIOMAS_idx` (`id_idioma`),
  KEY `FK_DSCOGRAFICAS_idx` (`id_discografica`),
  CONSTRAINT `FK_COMPRADORES` FOREIGN KEY (`id_comprador`) REFERENCES `compradores` (`id_comprador`),
  CONSTRAINT `FK_DSCOGRAFICAS` FOREIGN KEY (`id_discografica`) REFERENCES `discograficas` (`id_discografica`),
  CONSTRAINT `FK_IDIOMAS` FOREIGN KEY (`id_idioma`) REFERENCES `idiomas` (`id_idioma`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cds`
--

LOCK TABLES `cds` WRITE;
/*!40000 ALTER TABLE `cds` DISABLE KEYS */;
INSERT INTO `cds` VALUES (1,'(What\'s the Story) Morning Glory?',4,2,7,2019),(2,'Definitely Maybe',2,2,6,2018),(3,'The Magic Whip 2',4,2,5,2017),(4,'The Great Escape',5,2,4,2019),(5,'Quién me ha visto...',4,1,3,2018),(6,'Cuando el río suena',8,1,5,2000),(7,'Rara',3,1,5,2001),(8,'Bienvenido a mi habitación',12,1,2,2006),(9,'Empath',14,2,1,2007),(10,'Epicloud',12,2,4,2018),(11,'Oh la la',14,4,3,2016),(12,'Mon amour',4,4,6,2014),(13,'D\'eux',3,4,5,2016),(14,'D\'elles',2,4,2,2013),(15,'Reise, Reise',5,9,1,2004),(16,'Klavier',6,9,4,2004),(17,'Vientos del pueblo',9,1,7,2018),(18,'Foc',5,1,5,2007),(19,'Alchemy',7,6,6,2019),(20,'Pereval',10,6,3,2007),(21,'Altai Sayan Tandy',11,6,2,2004),(22,'Koshkyn',13,6,1,2016),(23,'Кайфмэн (Kaifman)',12,6,4,2006),(24,'The Magic Whip',14,2,5,2018),(25,'Fever Dream',1,2,2,2004),(26,'Beneath the skin',10,2,3,2006),(27,'Spice',4,2,6,2008),(28,'SpiceWorld',5,2,5,2004),(29,'Moriagatteyo',8,5,2,2019),(30,'하드캐리 (Hard candy)',9,5,1,2018),(31,'Teen age',6,5,4,2017),(32,'Love and letter',3,5,7,2016),(33,'Walls of Jericho',7,2,5,2015),(34,'7 Sinners',1,2,4,2013),(35,'Fuente y caudal',11,1,1,2004),(37,'PRUEBA',1,1,1,1991),(38,'Jesucristo SuperStar',NULL,NULL,7,2033),(39,'ola k ase',NULL,NULL,3,1969);
/*!40000 ALTER TABLE `cds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cds_generos`
--

DROP TABLE IF EXISTS `cds_generos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cds_generos` (
  `id_cd` int(11) NOT NULL,
  `id_genero` int(11) NOT NULL,
  PRIMARY KEY (`id_cd`,`id_genero`),
  KEY `FK_CDS_GENEROS_GENEROS_idx` (`id_genero`),
  CONSTRAINT `FK_CDS_GENEROS_CDS` FOREIGN KEY (`id_cd`) REFERENCES `cds` (`id_cd`),
  CONSTRAINT `FK_CDS_GENEROS_GENEROS` FOREIGN KEY (`id_genero`) REFERENCES `generos` (`id_genero`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cds_generos`
--

LOCK TABLES `cds_generos` WRITE;
/*!40000 ALTER TABLE `cds_generos` DISABLE KEYS */;
INSERT INTO `cds_generos` VALUES (5,1),(10,1),(16,1),(29,1),(4,2),(11,2),(17,2),(20,2),(27,2),(1,3),(7,3),(13,3),(19,3),(32,3),(2,4),(9,4),(15,4),(30,4),(33,4),(3,5),(8,5),(14,5),(21,5),(26,5),(6,6),(12,6),(18,6),(25,6),(31,6),(35,7),(22,8),(23,8),(24,8),(28,8),(34,8);
/*!40000 ALTER TABLE `cds_generos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compradores`
--

DROP TABLE IF EXISTS `compradores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `compradores` (
  `id_comprador` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) DEFAULT NULL,
  `apellidos` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_comprador`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compradores`
--

LOCK TABLES `compradores` WRITE;
/*!40000 ALTER TABLE `compradores` DISABLE KEYS */;
INSERT INTO `compradores` VALUES (1,'Marta','Serra'),(2,'Laura','Masdeu'),(3,'Juan','Grau'),(4,'Anna','Buchanan'),(5,'Pepi','Barberan'),(6,'Mar','Talavera'),(7,'Jordi','Madrid'),(8,'Vanessa','Stark'),(9,'Kevin','Buchanan'),(10,'Oriol','Milà'),(11,'Eva','Armenteros'),(12,'Diego','Diegos'),(13,'Alberto','Porcel'),(14,'Dani','Puzzle');
/*!40000 ALTER TABLE `compradores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discograficas`
--

DROP TABLE IF EXISTS `discograficas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `discograficas` (
  `id_discografica` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id_discografica`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discograficas`
--

LOCK TABLES `discograficas` WRITE;
/*!40000 ALTER TABLE `discograficas` DISABLE KEYS */;
INSERT INTO `discograficas` VALUES (1,'Sony'),(2,'Warner'),(3,'Universal'),(4,'EMI'),(5,'StarMakers'),(6,'Polygram'),(7,'The Cool discography');
/*!40000 ALTER TABLE `discograficas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `generos`
--

DROP TABLE IF EXISTS `generos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `generos` (
  `id_genero` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id_genero`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `generos`
--

LOCK TABLES `generos` WRITE;
/*!40000 ALTER TABLE `generos` DISABLE KEYS */;
INSERT INTO `generos` VALUES (1,'Rock'),(2,'Pop'),(3,'Heavy Metal'),(4,'Punk'),(5,'Indie'),(6,'Reggea'),(7,'Flamenco'),(8,'Folk'),(9,'Rap');
/*!40000 ALTER TABLE `generos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `idiomas`
--

DROP TABLE IF EXISTS `idiomas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `idiomas` (
  `id_idioma` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id_idioma`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `idiomas`
--

LOCK TABLES `idiomas` WRITE;
/*!40000 ALTER TABLE `idiomas` DISABLE KEYS */;
INSERT INTO `idiomas` VALUES (1,'Español'),(2,'Inglés'),(3,'Italiano'),(4,'Francés'),(5,'Koreano'),(6,'Ruso'),(7,'Chino'),(8,'Japonés'),(9,'Alemán');
/*!40000 ALTER TABLE `idiomas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-17 13:52:03
