from clase.genero import *
from db.admingenero import *

from clase.discografica import *
from db.admindiscografica import *


opcion = -1
while opcion != "0":
    print("""
------- Menú Principal -------

1. Administrar Géneros
2. Administrar Discográficas

0. Salir
------------------------------
""")
    opcion = input("Dime qué opción desea: ")
    if opcion == "1":
        opcion_genero = -1
        while opcion_genero != "0":
            print('''
-------- Menú Género -------
1.- Insertar
2.- Actualizar
3.- Listar
4.- Borrar

0.- Volver al Menú Principal

---------------------
        ''')
            opcion_genero = input("Dime qué opción desea: ")

            if opcion_genero == "1":
                genero = input("Dígame un género: ")
                genero1 = Genero(genero)
                administrador1 = AdminGenero()
                administrador1.insertar(genero1)
                print("Género introducido, gracias maj@")

            elif opcion_genero == "2":
                administrador1 = AdminGenero()
                administrador1.listar()
                print(" ")
                genero_viejo = input("Dígame el género que quiera cambiar: ")
                genero_nuevo = input("Dígame el nuevo nombre: ")
                genero_viejo1 = Genero(genero_viejo)
                genero_nuevo1 = Genero(genero_nuevo)
                administrador1.actualizar(genero_viejo1, genero_nuevo1)
                print("Género modificado, gracias maj@")

            elif opcion_genero == "3":
                administrador1 = AdminGenero()
                administrador1.listar()

            elif opcion_genero == "4":
                administrador1 = AdminGenero()
                administrador1.listar()
                print(" ")
                genero = input("Dígame un género a borrar: ")
                genero1 = Genero(genero)
                administrador1.borrar(genero1)
                print("Género borrado, gracias maj@")

            elif opcion_genero == "0":
                print("Muchas gracias, regresando a Menú Principal... ")
                administrador_cerrar = AdminGenero()
                administrador_cerrar.cerrarConexion()
            else:
                print("Error, indique una opción válida")

    elif opcion == "2":
        opcion_dicografica = -1
        while opcion != "0":
            print('''
-------- Menú Discográfica-------
1.- Insertar
2.- Actualizar
3.- Listar
4.- Borrar

0.- Volver al Menú Principal

---------------------
        ''')
            opcion_discografica = input("Dime qué opción desea: ")

            if opcion_discografica == "1":
                discografica = input("Dígame una discografica: ")
                discografica1 = Discografica(discografica)
                administrador1 = AdminDiscografica()
                administrador1.insertar(discografica1)
                print("Discográfica introducida, gracias maj@")

            elif opcion_discografica == "2":
                administrador1 = AdminDiscografica()
                administrador1.listar()
                print(" ")
                discografica_viejo = input("Dígame la discografica que quiera cambiar: ")
                discografica_nuevo = input("Dígame el nuevo nombre: ")
                discografica_viejo1 = Discografica(discografica_viejo)
                discografica_nuevo1 = Discografica(discografica_nuevo)
                administrador1.actualizar(discografica_viejo1, discografica_nuevo1)
                print("Discográfica modificada, gracias maj@")

            elif opcion_discografica == "3":
                administrador1 = AdminDiscografica()
                administrador1.listar()

            elif opcion_discografica == "4":
                administrador1 = AdminDiscografica()
                administrador1.listar()
                print(" ")
                discografica = input("Dígame una discografica a borrar: ")
                discografica1 = Discografica(discografica)
                administrador1.borrar(discografica1)
                print("Discográfica borrada, gracias maj@")

            elif opcion_discografica == "0":
                print("Muchas gracias, regresando a Menú Principal... ")
                administrador_cerrar = AdminDiscografica()
                administrador_cerrar.cerrarConexion()
            else:
                print("Error, indique una opción válida")

    elif opcion == "0":
        print("Muchas gracias, saliendo... ")

    else:
        print("Error, indique una opción válida del menú principal")        
