
import mysql.connector

class AdminGenero:
	def __init__(self):
		self.__cnx = mysql.connector.connect(user='root', password='root', host='localhost', database='coleccion_cds')


	def insertar(self, genero): #YA FUNCIONA
		cursor = self.__cnx.cursor()
		query = "INSERT INTO generos (nombre) VALUES ('%s')" %(genero.getNombre()) 
		cursor.execute(query)
		self.__cnx.commit()
		cursor.close()

	def actualizar(self, nombre_viejo, nombre_nuevo): #YA FUNCIONA 
		cursor = self.__cnx.cursor()
		query = "UPDATE generos SET nombre = '%s' WHERE nombre like '%s'" % (nombre_nuevo.getNombre(), nombre_viejo.getNombre())
		cursor.execute(query)
		self.__cnx.commit()
		cursor.close()

	def listar(self):
		cursor = self.__cnx.cursor()
		query = "SELECT nombre FROM generos"
		cursor.execute(query)
		lista_generos = cursor.fetchall()

		for genero in lista_generos:
		    print('Género: ' + genero[0])

		cursor.close()

	def borrar(self, genero): #Preguntar en menu id del genero a borrar
		cursor = self.__cnx.cursor()
		query = "DELETE FROM generos WHERE nombre like '%s'" % (genero.getNombre())
		cursor.execute(query)
		self.__cnx.commit()		                    
		cursor.close()


	def cerrarConexion(self):
		self.__cnx.close() 

	def getByID(self, id):
		cursor = self.__cnx.cursor()		
		query = "SELECT nombre FROM generos WHERE id_genero = %i" %(id)
		cursor.execute(query)
		return cursor.fetchall()









