
from clase.discografica import *
from db.admindiscografica import *



import mysql.connector

class AdminDiscografica:
	def __init__(self):
		self.__cnx = mysql.connector.connect(user='root', password='root', host='localhost', database='coleccion_cds')


	def insertar(self, discografica):
		cursor = self.__cnx.cursor()
		query = "INSERT INTO discograficas (nombre) VALUES ('%s')" %(discografica.getNombre()) 
		cursor.execute(query)
		self.__cnx.commit()
		cursor.close()

	def actualizar(self, nombre_viejo, nombre_nuevo): 
		cursor = self.__cnx.cursor()
		query = "UPDATE discograficas SET nombre = '%s' WHERE nombre like '%s'" % (nombre_nuevo.getNombre(), nombre_viejo.getNombre())
		cursor.execute(query)
		self.__cnx.commit()
		cursor.close()

	def listar(self):#incorrecta (deberiamos hacer tipo funcion getall)
		cursor = self.__cnx.cursor()
		query = "SELECT nombre FROM discograficas"
		cursor.execute(query)
		lista_discograficas = cursor.fetchall()

		for discografica in lista_discograficas:
		    print('Discográfica: ' + discografica[0])

		cursor.close()

	def borrar(self, discografica):
		cursor = self.__cnx.cursor()
		query = "DELETE FROM discograficas WHERE nombre like '%s'" % (discografica.getNombre())
		cursor.execute(query)
		self.__cnx.commit()		                    
		cursor.close()


	def cerrarConexion(self):
		self.__cnx.close() 


	def listarDiscograficas(self):
		cursor = self.__cnx.cursor()
		query = "SELECT * FROM discograficas"
		cursor.execute(query)
		lista_discograficas = cursor.fetchall()
		interrogante = "Sony"
		for discografica in lista_discograficas:
			if interrogante in lista_discograficas:
				print(str(discografica[0]))

		#for discografica in lista_discograficas:
		#    return('Id: ' + str(discografica[0]) + '. Discográfica: ' + discografica[1])


	def getAll(self):#añadimos a una nueva lista los datos de la tabla discografica (luego la lista la metemos 
	# en otra lista en el archivo programa y hacemos un for que imprima los valores) 
		cursor = self.__cnx.cursor()
		query = "SELECT * FROM discograficas"
		cursor.execute(query)
		lista_d= cursor.fetchall()
		lista_discograficas = []

		for discografica in lista_d:
			objeto_discografica = Discografica(str(discografica[0]), discografica[1])
			lista_discograficas.append(objeto_discografica)
		return lista_discograficas

	def getByID(self, id):
		cursor = self.__cnx.cursor()		
		query = "SELECT nombre FROM discograficas WHERE id_discografica = %i" %(id)
		cursor.execute(query)
		return cursor.fetchall()

