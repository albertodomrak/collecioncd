
import mysql.connector

class AdminCd:
	def __init__(self):
		self.__cnx = mysql.connector.connect(user='root', password='root', host='localhost', database='coleccion_cds')


	def insertar(self, cd): #crear objeto cd(atribuirle titulo, anno) y un objeto discografica (nombre)
		cursor = self.__cnx.cursor()
		query = "INSERT INTO cds (titulo, id_discografica, anno) VALUES ('%s', %i, %i)" %(cd.getTitulo(), int(cd.getDiscografica().getID()), cd.getAnno()) 
		cursor.execute(query)
		self.__cnx.commit()
		cursor.close()

	def actualizar(self, cd_viejo, cd_nuevo):
		cursor = self.__cnx.cursor()
		query = "UPDATE cds SET titulo = '%s', id_discografica = %i, anno = %i WHERE titulo LIKE '%s' AND id_discografica = %i AND anno = %i" %(cd_nuevo.getTitulo(), int(cd_nuevo.getDiscografica().getID()), cd_nuevo.getAnno(), cd_viejo.getTitulo(), int(cd_viejo.getDiscografica().getID()), cd_viejo.getAnno())
		print(query)
		cursor.execute(query)
		self.__cnx.commit()
		cursor.close()

	def listar(self):
		cursor = self.__cnx.cursor()
		query = "SELECT titulo, id_discografica, anno FROM cds"
		cursor.execute(query)
		lista_cds = cursor.fetchall()

		for cd in lista_cds:
		    print('Título: ' + cd[0] + '. Discográfica: '+ str(cd[1]) + '. Año: ' + str(cd[2]))
		cursor.close()

	def borrar(self, genero): #Preguntar en menu id del genero a borrar
		cursor = self.__cnx.cursor()
		query = "DELETE FROM generos WHERE nombre like '%s'" % (genero.getNombre())
		cursor.execute(query)
		self.__cnx.commit()		                    
		cursor.close()


	def cerrarConexion(self):
		self.__cnx.close() 

	def comprobar(self):
		cursor = self.__cnx.cursor()
		query = "SELECT titulo, id_discografica, anno FROM cds"
		cursor.execute(query)
		lista_cds = cursor.fetchall()

		for cd in lista_cds:
		    return('Título: ' + cd[0] + '. Discográfica: '+ str(cd[1]) + '. Año: ' + str(cd[2]))
		cursor.close()

	def getByID(self, id):
		cursor = self.__cnx.cursor()		
		query = "SELECT nombre FROM cds WHERE id_cd = %i" %(id)
		cursor.execute(query)
		return cursor.fetchall()
