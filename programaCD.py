from clase.genero import *
from db.admingenero import *

from clase.discografica import *
from db.admindiscografica import *

from clase.cd import *
from db.admincd import *



opcion_genero = -1
while opcion_genero != "0":
    print('''
-------- Menú CDs -------
1.- Insertar
2.- Actualizar
3.- Listar
4.- Borrar

0.- Volver al Menú Principal

---------------------
''')
    opcion_genero = input("Dime qué opción desea: ")

    if opcion_genero == "1": #crear objeto cd(atribuirle titulo, anno), hacemo
    	titulo_cd = input("Dígame el título: ")
    	anno_cd = int(input("Dígame el año: "))
    	cd1 = Cd(titulo_cd, anno_cd)
    	administradorDiscografica = AdminDiscografica()
    	administrador1 = AdminCd()
    	lista_discograficas=administradorDiscografica.getAll()
    	for discografica in lista_discograficas:
    		print(discografica)
    	seleccion_discografica = int(input("Seleccione una discográfica: "))
    	nombre_discografica = administradorDiscografica.getByID(seleccion_discografica)
    	discografica1 = Discografica(nombre_discografica[0], seleccion_discografica)#tenemos que poner este cero porque siempre devuelve una tabla
    	cd1.setDiscografica(discografica1)
    	administrador1.insertar(cd1)
    	print("Cd introducido, gracias maj@")#podemos mejorarlo mostrando funcion str con los datos introducidos


    elif opcion_genero == "2":
        titulo_viejo = input("Dígame el título del cd que quiera modificar: ")
        anno_viejo = int(input("Dígame el año que quiera modificar: "))
        administradorDiscografica = AdminDiscografica()
        lista_discograficas=administradorDiscografica.getAll()
        for discografica in lista_discograficas:
            print(discografica)
        discografica_viejo = int(input("Indique la discografica a modificar: "))

        titulo_nuevo = input("Dígame el nuevo título: ")
        anno_nuevo = int(input("Dígame el nuevo año: "))
        administradorDiscografica = AdminDiscografica()
        lista_discograficas=administradorDiscografica.getAll()
        for discografica in lista_discograficas:
            print(discografica)
        discografica_nuevo = int(input("Indique la nueva discografica: "))


        cd_viejo = Cd(titulo_viejo, anno_viejo)

        administradorDiscografica = AdminDiscografica()
        nombre_discografica_viejo = administradorDiscografica.getByID(discografica_viejo)
        discografica1 = Discografica(nombre_discografica_viejo[0], discografica_viejo)#tenemos que poner este cero porque siempre devuelve una tabla
        cd_viejo.setDiscografica(discografica1)

        cd_nuevo = Cd(titulo_nuevo, anno_nuevo)

        nombre_discografica_nuevo = administradorDiscografica.getByID(discografica_nuevo)
        discografica2 = Discografica(nombre_discografica_nuevo[0], discografica_nuevo)#tenemos que poner este cero porque siempre devuelve una tabla
        cd_nuevo.setDiscografica(discografica2)


        administrador1 = AdminCd()
        administrador1.actualizar(cd_viejo, cd_nuevo)

        print("CD modificado, gracias maj@")

    elif opcion_genero == "3":
        administrador1 = AdminCd()
        administrador1.listar()

    elif opcion_genero == "4":
        administrador1 = AdminGenero()
        administrador1.listar()
        print(" ")
        genero = input("Dígame un género a borrar: ")
        genero1 = Genero(genero)
        administrador1.borrar(genero1)
        print("Género borrado, gracias maj@")

    elif opcion_genero == "0":
        print("Muchas gracias, regresando a Menú Principal... ")
        administrador_cerrar = AdminGenero()
        administrador_cerrar.cerrarConexion()
    else:
        print("Error, indique una opción válida")
