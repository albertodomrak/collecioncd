class Genero:
    def __init__(self,  nombre, id_genero = None):
        self.__id_genero = id_genero
        self.__nombre = nombre

    def setID(self, id_genero):
        self.__id_genero = id_genero

    def getID(self):
        return self.__id_genero

    def setNombre(self, nombre):
        self.__nombre = nombre

    def getNombre(self):
        return self.__nombre

    def __str__(self):
        return str(self.__id_genero) + " " + self.__nombre



