class Cd:
    def __init__(self,  titulo, anno, id_cd = None):
        self.__id_cd = id_cd
        self.__titulo = titulo
        self.__anno = anno

    def setID(self, id_cd):
        self.__id_cd = id_cd

    def getID(self):
        return self.__id_cd

    def setTitulo(self, titulo):
        self.__titulo = titulo

    def getTitulo(self):
        return self.__titulo

    def setAnno(self, anno):
        self.__anno = anno

    def getAnno(self):
        return self.__anno

    def setDiscografica(self, discografica):
        self.__discografica = discografica

    def getDiscografica(self):
        return self.__discografica

    def __str__(self):
        return str(self.__id_cd) + ". Título: " + self.__titulo + ". Año: " + str(self.__anno)

